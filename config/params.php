<?php

return [
    'twitch' => [
        'tokenUrl' => 'https://api.twitch.tv/kraken/oauth2/token?client_id=o2xpywe1rq84942wtub4rhats0p3sw&client_secret=nhi4g7m6z58ppw659udi975wfs7vec&grant_type=client_credentials',
        'clientId' => 'o2xpywe1rq84942wtub4rhats0p3sw',
        'clientSecret' => 'nhi4g7m6z58ppw659udi975wfs7vec',
        'grant_type' => 'client_credentials',
        'streamsUrl' => 'https://api.twitch.tv/helix/streams',
        // frequency of requests to twitch service (min)
        'requestsFrequency' => 5,
    ],
    'api' => [
        // ip-addresses for access to api
        'ips' => [
            '127.0.0.1',
        ],
    ],
    
];