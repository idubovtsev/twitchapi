1 Выполнить миграции и php artisan passport:install
2 Изменить настройки, если требуется, в файле /config/params.php
3 Добавить в крон задание  * * * * * php .../artisan schedule:run >> /dev/null 2>&1
4 Запоросить токен авторизации

curl -X POST \
  http://site.loc/oauth/token \
  -F grant_type=client_credentials \
  -F client_id=1 \
  -F client_secret=QMzAlKMWTwfV8gZ90vg4dUcsCz4YZmbA531yqkbO\

  или в коде

$response = $client->post('http://site.loc/oauth/token', [
  'form_params' => [
      'grant_type' => 'client_credentials',
      'client_id' => '1',
      'client_secret' => 'QMzAlKMWTwfV8gZ90vg4dUcsCz4YZmbA531yqkbO',
  ],
]);

$token = json_decode($response->getBody(), true)['access_token'];

5 Выполнить запрос данных с заголовками:
curl -X GET \
  http://site.loc/api/twitch/streams \
  -H 'Accept: application/json' \
  -H 'Authorization: Bearer <$token>' \

API 1
возвращает все потоки, которые были в заданный интервал времени и пиковое значение зрителей
Запросы вида:
curl -X GET \
  http://site.loc/api/twitch/streams?game_id[]=138585&game_id[]=32399&now=true \
  -H 'Accept: application/json' \
  -H 'Authorization: Bearer <$token>' \

Параметры:
game_id - id игры из сервиса twitch (может задаваться массивом game_id[]= или единичным значением game_id)
time_from - метка времени в формате unix (1513202726) левая граница интервала выборки
time_to - метка времени в формате unix (1513202726) правая граница интервала выборки
now - (boolean) если true, то берутся только последние данные (в интервале времени между выгрузками), а значения time_from, time_to будут проигнорированы
cursor - указывается для выборки следующих 100 строк, берется из предыдущего запроса
channel_id - id канала сервиса twitch
streamer_id - id юзера сервиса twitch

Ответ
{
    "data": [
        {
            "channel_id":"26919017168",
            "streamer_id":"64291396",
            "game_id":"138585",
            "max_viewers":170
        },
        {
            "channel_id":"26928501424",
            "streamer_id":"73419051",
            "game_id":"138585",
            "max_viewers":170
        },
    ],
    "cursor": "1513201854p2"
}

Если ничего не найдено возвращается статус 204

API 2
Возвращает общее количество зрителей по каждой игре
Запросы вида:
curl -X GET \
  http://site.loc/api/twitch/streams?viewer-count?game_id[]=138585&game_id[]=32399&time_to=1513205000 \
  -H 'Accept: application/json' \
  -H 'Authorization: Bearer <$token>' \

game_id - id игры из сервиса twitch (может задаваться массивом game_id[]= или единичным значением game_id)
time_from - метка времени в формате unix (1513202726) левая граница интервала выборки
time_to - метка времени в формате unix (1513202726) правая граница интервала выборки
now - (boolean) если true, то берутся только последние данные (в интервале времени между выгрузками), а значения time_from, time_to будут проигнорированы
cursor - указывается для выборки следующих 100 строк, берется из предыдущего запроса

Ответ:
{
    "data":[
        {
            "game_id":"138585",
            "created_at":"2017-12-14 00:58:54",
            "sum_viewers":"69470"
        },
        {
            "game_id":"138585",
            "created_at":"2017-12-14 01:45:27",
            "sum_viewers":"76887"
        },
    ],
    "cursor":"1513209279p2"
}