<?php

namespace App\Http\Middleware;

use Closure;

class IpMiddleware
{
    /**
     * IP based access to the methods
     * array with ip - params.api.ips
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!in_array($request->ip(), config('params.api.ips')))
            abort(403, 'Access for your IP address is forbidden');
        return $next($request);
    }
}
