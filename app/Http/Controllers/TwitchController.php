<?php

namespace App\Http\Controllers;

use App\Eloquent\StreamViewer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TwitchController extends Controller
{
    // max number of items per page
    const PAGINATE = 100;
    public function __construct()
    {
        $this->middleware('ipcheck');
        $this->middleware('client');
    }

    public function streams(Request $request)
    {
        $data = StreamViewer::getStreamsData($request);

        if (!count($data['data'])) {
            return response()->json(null, 204);
        } else {
            return $data;
        }
    }

    public function viewerCount(Request $request)
    {
        $data = StreamViewer::getViewerCountData($request);

        if (!count($data['data'])) {
            return response()->json(null, 204);
        } else {
            return $data;
        }
    }
}
