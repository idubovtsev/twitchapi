<?php

namespace App\Console\Commands;

use App\Eloquent\Game;
use App\Eloquent\StreamViewer;
use App\Models\TwitchApi;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

/**
 * Class Twitch
 * @package App\Console\Commands
 */
class Twitch extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'twitch:get';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command for getting data from twitch service';

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // запрет одновременного запуска
        $file='/tmp/api_twitch.lock';
        if(!flock($lock_file = fopen($file, 'w'), LOCK_EX | LOCK_NB)) {
            die("Already running");
        }
        
        $twitchApi = new TwitchApi();
        $gamesIds = Game::query()->select('id')->get()->toArray();

        foreach($gamesIds as $item) {
            $returned = $twitchApi->getStreamsInformationByGameId($item['id']);
            if (!empty($returned) && is_array($returned)) {
                $batchArray = array_map(function ($stream) {
                    return [
                        'channel_id' => $stream['id'],
                        'streamer_id' => $stream['user_id'],
                        'game_id' => $stream['game_id'],
                        'service' => 'twitch',
                        'viewer_count' => $stream['viewer_count'],
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s'),
                    ];
                }, $returned);
                try {
                    StreamViewer::query()->insert($batchArray);
                } catch (\Exception $e) {
                    Log::warning('Twitch save data: ' . print_r($e, true));
                }

            }
        }
    }
}
