<?php

namespace App\Models;

use GuzzleHttp\Psr7\Request;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;

class TwitchApi extends Model
{
    const STATUS_SERVER_UNAVAILABLE = '503';
    const STATUS_MANY_REQUESTS = '429';
    const STATUS_OK = '200';

    // 100 - max
    public $firstCnt = 100;
    private $tokenUrl;
    private $streamsUrl;
    private $accessToken;
    private $http;
    
    public function __construct()
    {
        parent::__construct();
        $this->tokenUrl = config('params.twitch.tokenUrl');
        $this->streamsUrl = config('params.twitch.streamsUrl');
        $this->http = new Client();
        $this->getAccessToken();

    }

    /**
     * получение токена
     * @return mixed
     */
    protected function getAccessToken()
    {
        if (empty($this->accessToken)) {
            if (!empty(Cache::get('twitchAccessToken'))) {
                $this->accessToken = Cache::get('twitchAccessToken');
            } else {
                $this->getNewAccessToken();
            }
        }
    }

    /**
     * замена токена
     * request for twitch token
     */
    protected function getNewAccessToken()
    {
        $http = new Client();
        $response = $http->post($this->tokenUrl);
        $body = json_decode($response->getBody(), true);
        if (!empty($body['access_token'])) {
            $this->accessToken = $body['access_token'];
            $expressIn = !empty($body['expires_in']) ? intval($body['expires_in'] / 60) : null;
            Cache::add('twitchAccessToken', $this->accessToken, $expressIn);
        }
    }

    /**
     * Запрос
     * @param $queryString
     * @return Request
     */
    protected function getResponse($queryString)
    {
        return $this->http->get($this->streamsUrl . '?' . $queryString, [
            'headers' => [
                'Client-ID' => config('params.twitch.clientId'),
                'Authorization' => 'Bearer ' . $this->accessToken,
            ],
        ]);
    }

    /**
     * @param $id
     * @return array
     */
    public function getStreamsInformationByGameId($id)
    {
        $result = [];
        $cursor = '';
        do {
            $queryParams = [
                'first' => $this->firstCnt,
                'game_id' => $id,
                'after' => $cursor,
            ];
            $queryString = http_build_query($queryParams);
            $response = $this->getResponse($queryString);
            $status = $response->getStatusCode();

            if ($status !== 200) {
                // Too Many Requests
                if ($status === 429) {
                    sleep(1);
                } elseif ($status !== 503) {
                    $this->getNewAccessToken();
                }
                $response = $this->getResponse($queryString);
            }

            // after the second request
            $status = $response->getStatusCode();
            if ($status === 200) {
                $body = json_decode($response->getBody(), true);
                if (count($body['data']) < $this->firstCnt) {
                    $cursor = false;
                } else {
                    $cursor = !empty($body['pagination']['cursor']) ? $body['pagination']['cursor'] : false;
                }
                if (!empty($body['data']) && is_array($body['data'])) {
                    $result = array_merge($result, $body['data']);
                }
            } else {
                Log::warning('Twitch: ' . print_r($response, true));
            }
            
        } while (!empty($cursor));

        return $result;
    }
}
