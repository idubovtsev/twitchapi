<?php

namespace App\Eloquent;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class StreamViewer extends Model
{
    const PER_PAGE = 100;
    
    /**
     * курсор хранит информацию о времени первого запроса
     * на случай, если со времени первого запроса в таблицу добавились новые записи
     * @param string $timeTo
     * @param integer $nextPage
     * @return string
     */
    public static function getCursor($timeTo, $nextPage)
    {
        return strtotime($timeTo) . 'p' . $nextPage;
    }

    /**
     * Парсит курсор в дату и номер страницы
     * @param $cursor
     * @return array
     */
    public static function parseFromCursor($cursor)
    {
        $params = explode('p', $cursor);
        return [
            'time' => date('Y-m-d H:i:s', $params[0]),
            'page' => $params[1],
        ];
    }

    /**
     * данные для дат и курсора
     * @param $requestAll
     * @return array
     */
    public static function getQueryData(array $requestAll = null)
    {
        $timeFrom = !empty($requestAll['time_from']) ? date('Y-m-d H:i:s', $requestAll['time_from']) : null;
        $timeTo = !empty($requestAll['time_to']) ? date('Y-m-d H:i:s', $requestAll['time_to']) : null;
        $cursor = !empty($requestAll['cursor']) ? $requestAll['cursor'] : null;
        $now = !empty($requestAll['now']) ? $requestAll['now'] : null;

        $page = null;
        $skip = null;

        // если есть курсор, то берем зафиксированное в первом запросе время
        if ($cursor) {
            $timeTo = static::parseFromCursor($cursor)['time'];
            $page = static::parseFromCursor($cursor)['page'];
            $skip = ($page - 1) * static::PER_PAGE;
        } else {
            if (!$timeTo) {
                $timeTo = date('Y-m-d H:i:s');
            }
        }

        // еcли нужны текущие данные, то $timeFrom и $timeTo игнорируются
        if ($now) {
            if (!$cursor) {
                $timeTo = date('Y-m-d H:i:s');
            }
            // принимаем "сейчас", как интервал в минутах, с какой частотой делаются выгрузки из twitch
            // то есть берем последние данные
            $timeFrom = date('Y-m-d H:i:s', (strtotime($timeTo) - config('params.twitch.requestsFrequency') * 60));
        }

        return compact('timeFrom', 'timeTo', 'page', 'skip');
    }

    /**
     * выбирает потоки по условиям
     * @param $request
     * @return array
     */
    public static function getStreamsData(Request $request)
    {
        // берем все каналы, которые были активны в заданный промежуток времени
        $query = StreamViewer::query()
            ->select([
                'channel_id',
                'streamer_id',
                'game_id',
                DB::raw('MAX(viewer_count) max_viewers'),
            ])->groupBy('channel_id', 'streamer_id', 'game_id')
            ->orderBy(DB::raw('MAX(viewer_count)'), 'desc');

        extract(static::getQueryData($request->all()));

        // если задан интервал или хотя бы одна из границ интервала
        if (!empty($timeFrom)) {
            $query->where('created_at', '>=', $timeFrom);
        }

        if (!empty($timeTo)) {
            $query->where('created_at', '<=', $timeTo);
        }

        if ($request->has('channel_id')) {
            $query->where('channel_id', $request->input('channel_id'));
        }

        if ($request->has('streamer_id')) {
            $query->where('streamer_id', $request->input('streamer_id'));
        }

        if ($request->has('game_id')) {
            $query->where('game_id', $request->input('game_id'));
        }
        
        if (!empty($skip) && !empty($page)) {
            $query->skip($skip);
        } else {
            $page = 1;
        }

        $query->take(static::PER_PAGE);

        $data = $query->get();

        return [
            'data' => $data,
            'cursor' => static::getCursor($timeTo, ($page + 1)),
        ];
    }

    /**
     * выбирает сумму зрителей игр по всем потокам
     * @param Request $request
     * @return array
     */
    public static function getViewerCountData(Request $request)
    {
        $query = StreamViewer::query()
            ->select([
                'game_id',
                'created_at',
                DB::raw('SUM(viewer_count) sum_viewers'),
            ])->groupBy('game_id', 'created_at')
            ->orderBy('created_at', 'asc');

        extract(static::getQueryData($request->all()));

        // если задан интервал или хотя бы одна из границ интервала
        if (!empty($timeFrom)) {
            $query->where('created_at', '>=', $timeFrom);
        }

        if (!empty($timeTo)) {
            $query->where('created_at', '<=', $timeTo);
        }

        if ($request->has('game_id')) {
            $query->where('game_id', $request->input('game_id'));
        }

        if (!empty($skip) && !empty($page)) {
            $query->skip($skip);
        } else {
            $page = 1;
        }

        $query->take(static::PER_PAGE);

        $data = $query->get();

        return [
            'data' => $data,
            'cursor' => static::getCursor($timeTo, ($page + 1)),
        ];
    }
}
