<?php

namespace Tests\Unit;

use App\Eloquent\StreamViewer;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class StreamViewerTest extends TestCase
{
    /**
     * Проверка обратной совместимости методов получения курсора и парсинга курсора
     */
    public function testGetCursor()
    {
        $dateTime = '2017-12-12 22:22:22';
        $page = 4;
        $cursor = StreamViewer::getCursor($dateTime, $page);
        $params = StreamViewer::parseFromCursor($cursor);
        $this->assertEquals($dateTime, $params['time']);
        $this->assertEquals($page, $params['page']);
    }

    /**
     * Проверим, что курсор первичен над заданным временем
     */
    public function testGetQueryDataCursor()
    {
        $timeFrom = strtotime('2017-12-11 22:22:22');
        $timeTo = strtotime('2017-12-12 22:22:22');

        $cursor = '1513156942p2';
        $cursorTimeValue = '2017-12-13 12:22:22';

        $data = [
            'time_from' => $timeFrom,
            'time_to' => $timeTo,
            'cursor' => $cursor,
            'now' => true,
        ];

        $result = StreamViewer::getQueryData($data);
        $this->assertEquals($cursorTimeValue, $result['timeTo']);
    }

    /**
     * если время time_to не задано, то берется текущее время
     */
    public function testGetQueryDataTimeTo()
    {
        $unixTimeNow = time();

        $result = StreamViewer::getQueryData();
        $this->assertGreaterThanOrEqual($unixTimeNow, strtotime($result['timeTo']));
    }
}
