<?php

namespace Tests\Feature;

use App\Eloquent\StreamViewer;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class TwitchControllerTest extends TestCase
{
    // use WithoutMiddleware;

    public function setUp()
    {
        parent::setUp();
        Artisan::call('migrate');
    }

    /**
     * проверка авторизации
     */
    public function testAuthorization()
    {
        Artisan::call('passport:install');
        $this->seed('StreamViewerSeed');
        
        $oauthClient = DB::table('oauth_clients')->find(1);
        $response = $this->json('POST', 'oauth/token', [
                'grant_type' => 'client_credentials',
                'client_id' => $oauthClient->id,
                'client_secret' => $oauthClient->secret,
            ]);

        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                'access_token'
            ]);

        $token = json_decode($response->content(), true)['access_token'];

        $response2 = $this->json('GET', 'api/twitch/streams', [], [
            'Accept' => 'application/json',
            'Authorization' => 'Bearer ' . $token,
        ]);

        $response2
            ->assertStatus(200)
            ->assertJsonStructure([
                'data',
                'cursor',
            ]);
    }

    /**
     * Проверяет данные, игнорируя авторизацию
     */
    public function testDataWithoutAuth()
    {
        $this->seed('StreamViewerSeed');
        
        $this->withoutMiddleware();
        $response = $this->json('GET', 'api/twitch/streams', [], [
            'Accept' => 'application/json',
        ]);

        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                'data',
                'cursor',
            ]);
        
        $data = json_decode($response->content(), true);
        $this->assertEquals(5, count($data['data']));
        $this->assertEquals(200, $data['data'][0]['max_viewers']);

        $time_to = time() - 5 * 60;
        $response2 = $this->json('GET', 'api/twitch/streams?game_id=1&time_to=' . $time_to, [], [
            'Accept' => 'application/json',
        ]);

        $response2
            ->assertStatus(200)
            ->assertJsonStructure([
                'data',
                'cursor',
            ]);

        $data = json_decode($response2->content(), true);
        $this->assertEquals(2, count($data['data']));
    }

    /**
     * проверяет, что авторизация обломится
     */
    public function testWithoutAuthorization()
    {
        $this->json('GET', 'api/twitch/streams')
            ->assertStatus(401);
    }

    /**
     * проверяет корректность данных по количеству зрителей
     */
    public function testViewerCountWithoutAuth()
    {
        $this->seed('StreamViewerSeed');

        $this->withoutMiddleware();
        $response = $this->json('GET', 'api/twitch/viewer-count?game_id[]=1&now=1', [], [
            'Accept' => 'application/json',
        ]);

        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                'data',
                'cursor',
            ]);

        $data = json_decode($response->content(), true);
        $this->assertEquals(1, count($data['data']));

        $response = $this->json('GET', 'api/twitch/viewer-count?game_id[]=1', [], [
            'Accept' => 'application/json',
        ]);

        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                'data',
                'cursor',
            ]);

        $data = json_decode($response->content(), true);
        $this->assertEquals(2, count($data['data']));
        $this->assertEquals(290, $data['data'][0]['sum_viewers']);
    }
}
