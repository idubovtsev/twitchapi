<?php

use Illuminate\Database\Migrations\Migration;
use App\Eloquent\Game;

/**
 * insertion data for configuring games in DB
 * Class InsertGamesData
 */
class InsertGamesData extends Migration
{
    const DATA = [
        [
            'id' => '32399',
            'name' => 'Counter-Strike: Global Offensive',
        ],
        [
            'id' => '488615',
            'name' => 'Street Fighter V',
        ],
        [
            'id' => '491168',
            'name' => 'Clash Royale',
        ],
        [
            'id' => '496712',
            'name' => 'Call of Duty: WWII',
        ],
        [
            'id' => '9234',
            'name' => 'Casino',
        ],
        [
            'id' => '493057',
            'name' => 'PLAYERUNKNOWN\'S BATTLEGROUNDS',
        ],
        [
            'id' => '495589',
            'name' => 'FIFA 18',
        ],
        [
            'id' => '32982',
            'name' => 'Grand Theft Auto V',
        ],
        [
            'id' => '33214',
            'name' => 'Fortnite',
        ],
        [
            'id' => '138585',
            'name' => 'Hearthstone',
        ],
    ];
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Game::query()->insert(static::DATA);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        foreach (static::DATA as $item) {
            Game::destroy($item['id']);
        }
    }
}
