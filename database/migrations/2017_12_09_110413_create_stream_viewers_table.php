<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStreamViewersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stream_viewers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('channel_id');
            $table->string('streamer_id');
            $table->string('game_id');
            $table->string('service');
            $table->integer('viewer_count');
            $table->timestamps();

            $table->index(['game_id', 'created_at']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stream_viewers');
    }
}
