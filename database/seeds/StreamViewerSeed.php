<?php

use Illuminate\Database\Seeder;
use App\Eloquent\StreamViewer;

class StreamViewerSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        StreamViewer::query()->insert([
            [
                'channel_id' => '1',
                'streamer_id' => '1',
                'game_id' => '1',
                'service' => 'twitch',
                'viewer_count' => 100,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
            [
                'channel_id' => '2',
                'streamer_id' => '2',
                'game_id' => '1',
                'service' => 'twitch',
                'viewer_count' => 110,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
            [
                'channel_id' => '3',
                'streamer_id' => '3',
                'game_id' => '2',
                'service' => 'twitch',
                'viewer_count' => 120,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
            [
                'channel_id' => '1',
                'streamer_id' => '1',
                'game_id' => '1',
                'service' => 'twitch',
                'viewer_count' => 200,
                'created_at' => date('Y-m-d H:i:s', time() - 10 * 60),
                'updated_at' => date('Y-m-d H:i:s', time() - 10 * 60),
            ],
            [
                'channel_id' => '2',
                'streamer_id' => '2',
                'game_id' => '1',
                'service' => 'twitch',
                'viewer_count' => 90,
                'created_at' => date('Y-m-d H:i:s', time() - 10 * 60),
                'updated_at' => date('Y-m-d H:i:s', time() - 10 * 60),
            ],
            [
                'channel_id' => '4',
                'streamer_id' => '4',
                'game_id' => '3',
                'service' => 'twitch',
                'viewer_count' => 80,
                'created_at' => date('Y-m-d H:i:s', time() - 15 * 60),
                'updated_at' => date('Y-m-d H:i:s', time() - 15 * 60),
            ],
            [
                'channel_id' => '5',
                'streamer_id' => '5',
                'game_id' => '3',
                'service' => 'twitch',
                'viewer_count' => 50,
                'created_at' => date('Y-m-d H:i:s', time() - 15 * 60),
                'updated_at' => date('Y-m-d H:i:s', time() - 15 * 60),
            ],
            
        ]);
    }
}
